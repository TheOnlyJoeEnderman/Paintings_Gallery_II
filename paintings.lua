
-- 1x1

paintings_lib.register1x1("burning_coal", "Burning Coal", "pgii_1x1_burning_coal.png^paintings_lib_frame1x1.png")
paintings_lib.register1x1("cloudy_night", "Cloudy Night", "pgii_1x1_cloudy_night.png^paintings_lib_frame1x1.png")
paintings_lib.register1x1("desert_sun", "Desert Sun", "pgii_1x1_desert_sun.png^paintings_lib_frame1x1.png")
paintings_lib.register1x1("eye_of_universe", "Eye of Universe", "pgii_1x1_eye_of_universe.png^paintings_lib_frame1x1.png")
paintings_lib.register1x1("mundane_realization", "Mundane Realization", "pgii_1x1_mundane_realization.png^paintings_lib_frame1x1.png")
paintings_lib.register1x1("parrot_village", "Parrot Village", "pgii_1x1_parrot_village.png^paintings_lib_frame1x1.png")
paintings_lib.register1x1("purple_king", "Purple King", "pgii_1x1_purple_king.png^paintings_lib_frame1x1.png")
paintings_lib.register1x1("quantum_tea", "Quantum Tea", "pgii_1x1_quantum_tea.png^paintings_lib_frame1x1.png")
paintings_lib.register1x1("serene_sunset", "Serene Sunset", "pgii_1x1_serene_sunset.png^paintings_lib_frame1x1.png")
paintings_lib.register1x1("tulip_field", "Tulip Field", "pgii_1x1_tulip_field.png^paintings_lib_frame1x1.png")

-- 1x2

paintings_lib.register1x2("morning_beach", "Morning Beach", "pgii_1x2_morning_beach.png^paintings_lib_frame1x2.png")
paintings_lib.register1x2("neon_alley", "Neon Alley", "pgii_1x2_neon_alley.png^paintings_lib_frame1x2.png")
paintings_lib.register1x2("ocean_sunset", "Ocean Sunset", "pgii_1x2_ocean_sunset.png^paintings_lib_frame1x2.png")
paintings_lib.register1x2("ocean_wave", "Ocean Wave", "pgii_1x2_ocean_wave.png^paintings_lib_frame1x2.png")
paintings_lib.register1x2("spectral_guitarist", "Spectral Guitarist", "pgii_1x2_spectral_guitarist.png^paintings_lib_frame1x2.png")
paintings_lib.register1x2("to_the_clearing", "To The Clearing", "pgii_1x2_to_the_clearing.png^paintings_lib_frame1x2.png")

-- 2x1

paintings_lib.register2x1("cityscape_small", "Cityscape Small", "pgii_2x1_cityscape_small.png^paintings_lib_frame2x1.png")
paintings_lib.register2x1("red_path", "Red Path", "pgii_2x1_red_path.png^paintings_lib_frame2x1.png")
paintings_lib.register2x1("sea_of_stars", "Sea of Stars", "pgii_2x1_sea_of_stars.png^paintings_lib_frame2x1.png")
paintings_lib.register2x1("we_have_liftoff", "We Have Liftoff", "pgii_2x1_we_have_liftoff.png^paintings_lib_frame2x1.png")

-- 2x2

paintings_lib.register2x2("aquarium_fish", "Aquarium Fish", "pgii_2x2_aquarium_fish.png^paintings_lib_frame2x2.png")
paintings_lib.register2x2("brass_tree", "Brass Tree", "pgii_2x2_brass_tree.png^paintings_lib_frame2x2.png")
paintings_lib.register2x2("happy_corgi", "Happy Corgi", "pgii_2x2_happy_corgi.png^paintings_lib_frame2x2.png")
paintings_lib.register2x2("happy_moth", "Happy Moth", "pgii_2x2_happy_moth.png^paintings_lib_frame2x2.png")
paintings_lib.register2x2("japanese_moonrise", "Japanese Moonrise", "pgii_2x2_japanese_moonrise.png^paintings_lib_frame2x2.png")
paintings_lib.register2x2("lavender_carousel", "Lavender Carousel", "pgii_2x2_lavender_carousel.png^paintings_lib_frame2x2.png")
paintings_lib.register2x2("lost_wonderland", "Lost Wonderland", "pgii_2x2_lost_wonderland.png^paintings_lib_frame2x2.png")
paintings_lib.register2x2("nebulous_events", "Nebulous Events", "pgii_2x2_nebulous_events.png^paintings_lib_frame2x2.png")
paintings_lib.register2x2("peaceful_sleep", "Peaceful Sleep", "pgii_2x2_peaceful_sleep.png^paintings_lib_frame2x2.png")
paintings_lib.register2x2("port_town", "Port Town", "pgii_2x2_port_town.png^paintings_lib_frame2x2.png")
paintings_lib.register2x2("psychedelic_trip", "Psychedelic Trip", "pgii_2x2_psychedelic_trip.png^paintings_lib_frame2x2.png")
paintings_lib.register2x2("royal_highness", "Royal Highness", "pgii_2x2_royal_highness.png^paintings_lib_frame2x2.png")
paintings_lib.register2x2("superposition_cat", "Superposition Cat", "pgii_2x2_superposition_cat.png^paintings_lib_frame2x2.png")
paintings_lib.register2x2("the_florist_rests", "The Florist Rests", "pgii_2x2_the_florist_rests.png^paintings_lib_frame2x2.png")
paintings_lib.register2x2("tripping_cat", "Tripping Cat", "pgii_2x2_tripping_cat.png^paintings_lib_frame2x2.png")
paintings_lib.register2x2("tulip_valley", "Tulip Valley", "pgii_2x2_tulip_valley.png^paintings_lib_frame2x2.png")
paintings_lib.register2x2("where_the_phoenix_was", "Where the Phoenix Was", "pgii_2x2_where_the_phoenix_was.png^paintings_lib_frame2x2.png")

-- 3x2

paintings_lib.register3x2("brass_gears", "Brass Gears", "pgii_3x2_brass_gears.png^paintings_lib_frame3x2.png")
paintings_lib.register3x2("cabin_homestead", "Cabin Homestead", "pgii_3x2_cabin_homestead.png^paintings_lib_frame3x2.png")
paintings_lib.register3x2("crabby_space", "Crabby Space", "pgii_3x2_crabby_space.png^paintings_lib_frame3x2.png")
paintings_lib.register3x2("elven_scholar", "Elven Scholar", "pgii_3x2_elven_scholar.png^paintings_lib_frame3x2.png")
paintings_lib.register3x2("emotion_explosion", "Emotion Explosion", "pgii_3x2_emotion_explosion.png^paintings_lib_frame3x2.png")
paintings_lib.register3x2("flying_car", "Flying Car", "pgii_3x2_flying_car.png^paintings_lib_frame3x2.png")
paintings_lib.register3x2("haunted_library", "Haunted Library", "pgii_3x2_haunted_library.png^paintings_lib_frame3x2.png")
paintings_lib.register3x2("kite_festivities", "Kite Festivities", "pgii_3x2_kite_festivities.png^paintings_lib_frame3x2.png")
paintings_lib.register3x2("meditative_moment", "Meditative Moment", "pgii_3x2_meditative_moment.png^paintings_lib_frame3x2.png")
paintings_lib.register3x2("mushroom_planet", "Mushroom Planet", "pgii_3x2_mushroom_planet.png^paintings_lib_frame3x2.png")
paintings_lib.register3x2("summer_desert", "Summer Desert", "pgii_3x2_summer_desert.png^paintings_lib_frame3x2.png")
paintings_lib.register3x2("yellow_city", "Yellow City", "pgii_3x2_yellow_city.png^paintings_lib_frame3x2.png")

-- 3x3

paintings_lib.register3x3("angry_reviewer", "Angry Reviewer", "pgii_3x3_angry_reviewer.png^paintings_lib_frame3x3.png")
paintings_lib.register3x3("between_worlds", "Between Worlds", "pgii_3x3_between_worlds.png^paintings_lib_frame3x3.png")
paintings_lib.register3x3("cat_warrior", "Cat Warrior", "pgii_3x3_cat_warrior.png^paintings_lib_frame3x3.png")
paintings_lib.register3x3("closeup_moon", "Closeup Moon", "pgii_3x3_closeup_moon.png^paintings_lib_frame3x3.png")
paintings_lib.register3x3("cosmos_stares_back", "Cosmos Stares Back", "pgii_3x3_cosmos_stares_back.png^paintings_lib_frame3x3.png")
paintings_lib.register3x3("mistress_time", "Mistress of Time", "pgii_3x3_mistress_time.png^paintings_lib_frame3x3.png")
paintings_lib.register3x3("moonflower_bloom", "Moonflower Bloom", "pgii_3x3_moonflower_bloom.png^paintings_lib_frame3x3.png")

-- 4x2

paintings_lib.register4x2("absurdly_large_teacup", "Absurdly Large Teacup", "pgii_4x2_absurdly_large_teacup.png^paintings_lib_frame4x2.png")
paintings_lib.register4x2("city_of_fog", "City of Fog", "pgii_4x2_city_of_fog.png^paintings_lib_frame4x2.png")
paintings_lib.register4x2("coral_reef", "Coral Reef", "pgii_4x2_coral_reef.png^paintings_lib_frame4x2.png")
paintings_lib.register4x2("cosmic_kitty", "Cosmic Kitty", "pgii_4x2_cosmic_kitty.png^paintings_lib_frame4x2.png")
paintings_lib.register4x2("cybercar_parked", "Cybercar Parked", "pgii_4x2_cybercar_parked.png^paintings_lib_frame4x2.png")
paintings_lib.register4x2("deep_city", "Deep City", "pgii_4x2_deep_city.png^paintings_lib_frame4x2.png")
paintings_lib.register4x2("drinking_cat", "Drinking Cat", "pgii_4x2_drinking_cat.png^paintings_lib_frame4x2.png")
paintings_lib.register4x2("flooded_aqueduct", "Flooded Aqueduct", "pgii_4x2_flooded_aqueduct.png^paintings_lib_frame4x2.png")
paintings_lib.register4x2("frost_punk", "Frost Punk", "pgii_4x2_frost_punk.png^paintings_lib_frame4x2.png")
paintings_lib.register4x2("lily_pond", "Lily Pond", "pgii_4x2_lily_pond.png^paintings_lib_frame4x2.png")
paintings_lib.register4x2("lime_matrix", "Lime Matrix", "pgii_4x2_lime_matrix.png^paintings_lib_frame4x2.png")
paintings_lib.register4x2("purple_mountain", "Purple Mountain", "pgii_4x2_purple_mountain.png^paintings_lib_frame4x2.png")
paintings_lib.register4x2("rpg_cliff", "RPG Cliff", "pgii_4x2_rpg_cliff.png^paintings_lib_frame4x2.png")

-- 4x3

paintings_lib.register4x3("bioluminescent_shore", "Bioluminescent Shore", "pgii_4x3_bioluminescent_shore.png^paintings_lib_frame4x3.png")
paintings_lib.register4x3("goodnight_greenery", "Goodnight Greenery", "pgii_4x3_goodnight_greenery.png^paintings_lib_frame4x3.png")
paintings_lib.register4x3("landing_eagle", "Landing Eagle", "pgii_4x3_landing_eagle.png^paintings_lib_frame4x3.png")
paintings_lib.register4x3("princess_meeting", "Princess Meeting", "pgii_4x3_princess_meeting.png^paintings_lib_frame4x3.png")
paintings_lib.register4x3("water_background", "Water Background", "pgii_4x3_water_background.png^paintings_lib_frame4x3.png")

-- 4x4

paintings_lib.register4x4("abyssal_relic", "Abyssal Relic", "pgii_4x4_abyssal_relic.png^paintings_lib_frame4x4.png")
paintings_lib.register4x4("forest_music", "Forest Music", "pgii_4x4_forest_music.png^paintings_lib_frame4x4.png")
paintings_lib.register4x4("purple_maze", "Purple Maze", "pgii_4x4_purple_maze.png^paintings_lib_frame4x4.png")
paintings_lib.register4x4("quantum_garden", "Quantum Garden", "pgii_4x4_quantum_garden.png^paintings_lib_frame4x4.png")
paintings_lib.register4x4("trees_gathered", "Trees Gathered", "pgii_4x4_trees_gathered.png^paintings_lib_frame4x4.png")
paintings_lib.register4x4("trippy_shrooms", "Trippy Shrooms", "pgii_4x4_trippy_shrooms.png^paintings_lib_frame4x4.png")
