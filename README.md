# Paintings Gallery II: The Enhanced Exhibit

####
Using Paintings Library, I can make several art pieces. This is a showcase, both of my paintings API, and the increased performance of image generators. I have licensed the works CC0 as I believe every self respecting person using AI art should. I will not say I have got no better at prompting, but really, that makes less difference than the AI tool chosen. Luckily Tengr.AI is very open with the terms of use. They allow full ownership to the full legal extent (obviously minus trademark characters, etc) even with a free account. I also used ChatGPT 3.5 and 4 to help me use a broader set of words on prompts (as AI art greatly benefit from non-standard language) and for coming up with categories and ideas I simply could never have thought of with my limited range of art preferences.
####
AI artwork courtesy of: https://tengr.ai/
####
NOTICE: I have been told explicitly by the legal team from Tengr.AI that aside from any potential future legal changes to ownership of AI generated works, users own _all_ generated works under the current ToS (Terms of Service) and always will own said works. No future changes will negate current or future ownership.
