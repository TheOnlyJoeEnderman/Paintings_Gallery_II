if minetest.get_modpath("default") ~= nil then

	if minetest.get_modpath("dye") ~= nil then

		if minetest.get_modpath("wool") ~= nil then

-- The Basics

minetest.register_craft({
	output = "paintings_lib:1x1_blank1x1",
	recipe = {
		{"default:stick", "default:stick", "default:stick"},
		{"default:stick", "group:wool", "default:stick"},
		{"default:stick", "default:stick", "default:stick"},
	}
})

minetest.register_craft({
	output = "paintings_lib:1x2_blank1x2",
	recipe = {
		{"paintings_lib:1x1_blank1x1"},
		{"paintings_lib:1x1_blank1x1"},
	}
})

minetest.register_craft({
	output = "paintings_lib:2x1_blank2x1",
	recipe = {
		{"paintings_lib:1x1_blank1x1", "paintings_lib:1x1_blank1x1"},
	}
})

minetest.register_craft({
	output = "paintings_lib:2x2_blank2x2",
	recipe = {
		{"paintings_lib:1x1_blank1x1", "paintings_lib:1x1_blank1x1"},
		{"paintings_lib:1x1_blank1x1", "paintings_lib:1x1_blank1x1"},
	}
})

minetest.register_craft({
	output = "paintings_lib:3x2_blank3x2",
	recipe = {
		{"paintings_lib:1x1_blank1x1", "paintings_lib:1x1_blank1x1", "paintings_lib:1x1_blank1x1"},
		{"paintings_lib:1x1_blank1x1", "paintings_lib:1x1_blank1x1", "paintings_lib:1x1_blank1x1"},
	}
})

minetest.register_craft({
	output = "paintings_lib:3x3_blank3x3",
	recipe = {
		{"paintings_lib:1x1_blank1x1", "paintings_lib:1x1_blank1x1", "paintings_lib:1x1_blank1x1"},
		{"paintings_lib:1x1_blank1x1", "paintings_lib:1x1_blank1x1", "paintings_lib:1x1_blank1x1"},
		{"paintings_lib:1x1_blank1x1", "paintings_lib:1x1_blank1x1", "paintings_lib:1x1_blank1x1"},
	}
})

minetest.register_craft({
	output = "paintings_lib:4x2_blank4x2",
	recipe = {
		{"paintings_lib:2x2_blank2x2", "paintings_lib:2x2_blank2x2"},
	}
})

minetest.register_craft({
	output = "paintings_lib:4x3_blank4x3",
	recipe = {
		{"paintings_lib:3x2_blank3x2", "paintings_lib:3x2_blank3x2"},
	}
})

minetest.register_craft({
	output = "paintings_lib:4x4_blank4x4",
	recipe = {
		{"paintings_lib:2x2_blank2x2", "paintings_lib:2x2_blank2x2"},
		{"paintings_lib:2x2_blank2x2", "paintings_lib:2x2_blank2x2"},
	}
})

minetest.register_craft({
	output = "paintings_lib:4x4_blank4x4",
	recipe = {
		{"paintings_lib:4x2_blank4x2"},
		{"paintings_lib:4x2_blank4x2"},
	}
})

-- The Paintings

-- 1x1

minetest.register_craft({
	output = "paintings_lib:1x1_burning_coal",
	recipe = {
		{"paintings_lib:1x1_blank1x1", "dye:black"},
		{"", "dye:red"},
	}
})

minetest.register_craft({
	output = "paintings_lib:1x1_cloudy_night",
	recipe = {
		{"paintings_lib:1x1_blank1x1", "dye:black"},
		{"", "dye:grey"},
	}
})

minetest.register_craft({
	output = "paintings_lib:1x1_desert_sun",
	recipe = {
		{"paintings_lib:1x1_blank1x1", "dye:orange"},
		{"", "dye:orange"},
	}
})

minetest.register_craft({
	output = "paintings_lib:1x1_eye_of_universe",
	recipe = {
		{"paintings_lib:1x1_blank1x1", "dye:black"},
		{"", "dye:black"},
	}
})

minetest.register_craft({
	output = "paintings_lib:1x1_mundane_realization",
	recipe = {
		{"paintings_lib:1x1_blank1x1", "dye:grey"},
		{"", "dye:grey"},
	}
})

minetest.register_craft({
	output = "paintings_lib:1x1_parrot_village",
	recipe = {
		{"paintings_lib:1x1_blank1x1", "dye:dark_grey"},
		{"", "dye:yellow"},
	}
})

minetest.register_craft({
	output = "paintings_lib:1x1_purple_king",
	recipe = {
		{"paintings_lib:1x1_blank1x1", "dye:brown"},
		{"", "dye:violet"},
	}
})

minetest.register_craft({
	output = "paintings_lib:1x1_quantum_tea",
	recipe = {
		{"paintings_lib:1x1_blank1x1", "dye:magenta"},
		{"", "dye:green"},
	}
})

minetest.register_craft({
	output = "paintings_lib:1x1_serene_sunset",
	recipe = {
		{"paintings_lib:1x1_blank1x1", "dye:violet"},
		{"", "dye:pink"},
	}
})

minetest.register_craft({
	output = "paintings_lib:1x1_tulip_field",
	recipe = {
		{"paintings_lib:1x1_blank1x1", "dye:green"},
		{"", "dye:red"},
	}
})

-- 1x2

minetest.register_craft({
	output = "paintings_lib:1x2_burning_coal",
	recipe = {
		{"paintings_lib:1x2_blank1x2", "dye:yellow"},
		{"", "dye:cyan"},
	}
})

minetest.register_craft({
	output = "paintings_lib:1x2_neon_alley",
	recipe = {
		{"paintings_lib:1x2_blank1x2", "dye:cyan"},
		{"", "dye:magenta"},
	}
})

minetest.register_craft({
	output = "paintings_lib:1x2_ocean_sunset",
	recipe = {
		{"paintings_lib:1x2_blank1x2", "dye:blue"},
		{"", "dye:black"},
	}
})

minetest.register_craft({
	output = "paintings_lib:1x2_ocean_wave",
	recipe = {
		{"paintings_lib:1x2_blank1x2", "dye:white"},
		{"", "dye:blue"},
	}
})

minetest.register_craft({
	output = "paintings_lib:1x2_spectral_guitarist",
	recipe = {
		{"paintings_lib:1x2_blank1x2", "dye:violet"},
		{"", "dye:violet"},
	}
})

minetest.register_craft({
	output = "paintings_lib:1x2_sea_of_stars",
	recipe = {
		{"paintings_lib:1x2_blank1x2", "dye:violet"},
		{"", "dye:black"},
	}
})

minetest.register_craft({
	output = "paintings_lib:2x1_cityscape_small",
	recipe = {
		{"paintings_lib:2x1_blank2x1", "dye:grey"},
		{"", "dye:orange"},
	}
})

-- 2x1

minetest.register_craft({
	output = "paintings_lib:2x1_cityscape_small",
	recipe = {
		{"paintings_lib:2x1_blank2x1", "dye:grey"},
		{"", "dye:orange"},
	}
})

minetest.register_craft({
	output = "paintings_lib:2x1_red_path",
	recipe = {
		{"paintings_lib:2x1_blank2x1", "dye:brown"},
		{"", "dye:green"},
	}
})

minetest.register_craft({
	output = "paintings_lib:2x1_we_have_liftoff",
	recipe = {
		{"paintings_lib:2x1_blank2x1", "dye:orange"},
		{"", "dye:blue"},
	}
})

-- 2x2

minetest.register_craft({
	output = "paintings_lib:2x2_aquarium_fish",
	recipe = {
		{"paintings_lib:2x2_blank2x2", "dye:blue"},
		{"", "dye:orange"},
	}
})

minetest.register_craft({
	output = "paintings_lib:2x2_brass_tree",
	recipe = {
		{"paintings_lib:2x2_blank2x2", "dye:blue"},
		{"", "dye:yellow"},
	}
})

minetest.register_craft({
	output = "paintings_lib:2x2_happy_corgi",
	recipe = {
		{"paintings_lib:2x2_blank2x2", "dye:orange"},
		{"", "dye:white"},
	}
})

minetest.register_craft({
	output = "paintings_lib:2x2_happy_moth",
	recipe = {
		{"paintings_lib:2x2_blank2x2", "dye:brown"},
		{"", "dye:white"},
	}
})

minetest.register_craft({
	output = "paintings_lib:2x2_japanese_moorise",
	recipe = {
		{"paintings_lib:2x2_blank2x2", "dye:pink"},
		{"", "dye:blue"},
	}
})

minetest.register_craft({
	output = "paintings_lib:2x2_lavender_carousel",
	recipe = {
		{"paintings_lib:2x2_blank2x2", "dye:pink"},
		{"", "dye:pink"},
	}
})

minetest.register_craft({
	output = "paintings_lib:2x2_lost_wonderland",
	recipe = {
		{"paintings_lib:2x2_blank2x2", "dye:violet"},
		{"", "dye:magenta"},
	}
})

minetest.register_craft({
	output = "paintings_lib:2x2_nebulous_events",
	recipe = {
		{"paintings_lib:2x2_blank2x2", "dye:violet"},
		{"", "dye:pink"},
	}
})

minetest.register_craft({
	output = "paintings_lib:2x2_peaceful_sleep",
	recipe = {
		{"paintings_lib:2x2_blank2x2", "dye:dark_grey"},
		{"", "dye:blue"},
	}
})

minetest.register_craft({
	output = "paintings_lib:2x2_port_town",
	recipe = {
		{"paintings_lib:2x2_blank2x2", "dye:white"},
		{"", "dye:dark_grey"},
	}
})

minetest.register_craft({
	output = "paintings_lib:2x2_phychedelic_trip",
	recipe = {
		{"paintings_lib:2x2_blank2x2", "dye:orange"},
		{"", "dye:violet"},
	}
})

minetest.register_craft({
	output = "paintings_lib:2x2_royal_highness",
	recipe = {
		{"paintings_lib:2x2_blank2x2", "dye:yellow"},
		{"", "dye:red"},
	}
})

minetest.register_craft({
	output = "paintings_lib:2x2_superposition_cat",
	recipe = {
		{"paintings_lib:2x2_blank2x2", "dye:orange"},
		{"", "dye:orange"},
	}
})

minetest.register_craft({
	output = "paintings_lib:2x2_the_florist_rests",
	recipe = {
		{"paintings_lib:2x2_blank2x2", "dye:white"},
		{"", "dye:pink"},
	}
})

minetest.register_craft({
	output = "paintings_lib:2x2_tripping_cat",
	recipe = {
		{"paintings_lib:2x2_blank2x2", "dye:yellow"},
		{"", "dye:blue"},
	}
})

minetest.register_craft({
	output = "paintings_lib:2x2_tulip_valley",
	recipe = {
		{"paintings_lib:2x2_blank2x2", "dye:red"},
		{"", "dye:green"},
	}
})

minetest.register_craft({
	output = "paintings_lib:2x2_where_the_pheonix_was",
	recipe = {
		{"paintings_lib:2x2_blank2x2", "dye:red"},
		{"", "dye:yellow"},
	}
})

-- 3x2

minetest.register_craft({
	output = "paintings_lib:3x2_brass_gears",
	recipe = {
		{"paintings_lib:3x2_blank3x2", "dye:brown"},
		{"", "dye:brown"},
	}
})

minetest.register_craft({
	output = "paintings_lib:3x2_cabin_homestead",
	recipe = {
		{"paintings_lib:3x2_blank3x2", "dye:brown"},
		{"", "dye:yellow"},
	}
})

minetest.register_craft({
	output = "paintings_lib:3x2_crabby_space",
	recipe = {
		{"paintings_lib:3x2_blank3x2", "dye:blue"},
		{"", "dye:orange"},
	}
})

minetest.register_craft({
	output = "paintings_lib:3x2_elven_scholar",
	recipe = {
		{"paintings_lib:3x2_blank3x2", "dye:blue"},
		{"", "dye:grey"},
	}
})

minetest.register_craft({
	output = "paintings_lib:3x2_emotion_explosion",
	recipe = {
		{"paintings_lib:3x2_blank3x2", "dye:red"},
		{"", "dye:blue"},
	}
})

minetest.register_craft({
	output = "paintings_lib:3x2_flying_car",
	recipe = {
		{"paintings_lib:3x2_blank3x2", "dye:cyan"},
		{"", "dye:grey"},
	}
})

minetest.register_craft({
	output = "paintings_lib:3x2_haunted_library",
	recipe = {
		{"paintings_lib:3x2_blank3x2", "dye:brown"},
		{"", "dye:white"},
	}
})

minetest.register_craft({
	output = "paintings_lib:3x2_kite_festivities",
	recipe = {
		{"paintings_lib:3x2_blank3x2", "dye:blue"},
		{"", "dye:yellow"},
	}
})

minetest.register_craft({
	output = "paintings_lib:3x2_meditative_moment",
	recipe = {
		{"paintings_lib:3x2_blank3x2", "dye:green"},
		{"", "dye:brown"},
	}
})

minetest.register_craft({
	output = "paintings_lib:3x2_mushroom_planet",
	recipe = {
		{"paintings_lib:3x2_blank3x2", "dye:green"},
		{"", "dye:blue"},
	}
})

minetest.register_craft({
	output = "paintings_lib:3x2_summer_desert",
	recipe = {
		{"paintings_lib:3x2_blank3x2", "dye:pink"},
		{"", "dye:cyan"},
	}
})

minetest.register_craft({
	output = "paintings_lib:3x2_yellow_city",
	recipe = {
		{"paintings_lib:3x2_blank3x2", "dye:yellow"},
		{"", "dye:grey"},
	}
})

-- 3x3

minetest.register_craft({
    output = "paintings_lib:3x3_angry_reviewer",
    recipe = {
        {"paintings_lib:3x3_blank3x3", "dye:blue"},
        {"", "dye:black"},
    }
})

minetest.register_craft({
    output = "paintings_lib:3x3_between_worlds",
    recipe = {
        {"paintings_lib:3x3_blank3x3", "dye:green"},
        {"", "dye:dark_green"},
    }
})

minetest.register_craft({
    output = "paintings_lib:3x3_cat_warrior",
    recipe = {
        {"paintings_lib:3x3_blank3x3", "dye:blue"},
        {"", "dye:orange"},
    }
})

minetest.register_craft({
    output = "paintings_lib:3x3_closeup_moon",
    recipe = {
        {"paintings_lib:3x3_blank3x3", "dye:violet"},
        {"", "dye:dark_grey"},
    }
})

minetest.register_craft({
    output = "paintings_lib:3x3_cosmos_stares_back",
    recipe = {
        {"paintings_lib:3x3_blank3x3", "dye:black"},
        {"", "dye:blue"},
    }
})

minetest.register_craft({
    output = "paintings_lib:3x3_mistress_time",
    recipe = {
        {"paintings_lib:3x3_blank3x3", "dye:brown"},
        {"", "dye:dark_grey"},
    }
})

minetest.register_craft({
    output = "paintings_lib:3x3_moonflower_bloom",
    recipe = {
        {"paintings_lib:3x3_blank3x3", "dye:white"},
        {"", "dye:blue"},
    }
})

-- 4x2

minetest.register_craft({
	output = "paintings_lib:4x2_absurdly_large_teacup",
	recipe = {
		{"paintings_lib:4x2_blank4x2", "dye:blue"},
		{"", "dye:white"},
	}
})

minetest.register_craft({
	output = "paintings_lib:4x2_city_of_fog",
	recipe = {
		{"paintings_lib:4x2_blank4x2", "dye:blue"},
		{"", "dye:pink"},
	}
})

minetest.register_craft({
	output = "paintings_lib:4x2_coral_reef",
	recipe = {
		{"paintings_lib:4x2_blank4x2", "dye:blue"},
		{"", "dye:orange"},
	}
})

minetest.register_craft({
	output = "paintings_lib:4x2_cosmic_kitty",
	recipe = {
		{"paintings_lib:4x2_blank4x2", "dye:black"},
		{"", "dye:blue"},
	}
})

minetest.register_craft({
	output = "paintings_lib:4x2_cybercar_parked",
	recipe = {
		{"paintings_lib:4x2_blank4x2", "dye:magenta"},
		{"", "dye:cyan"},
	}
})

minetest.register_craft({
	output = "paintings_lib:4x2_deep_city",
	recipe = {
		{"paintings_lib:4x2_blank4x2", "dye:brown"},
		{"", "dye:orange"},
	}
})

minetest.register_craft({
	output = "paintings_lib:4x2_drinking_cat",
	recipe = {
		{"paintings_lib:4x2_blank4x2", "dye:brown"},
		{"", "dye:pink"},
	}
})

minetest.register_craft({
	output = "paintings_lib:4x2_flooded_aquaduct",
	recipe = {
		{"paintings_lib:4x2_blank4x2", "dye:cyan"},
		{"", "dye:grey"},
	}
})

minetest.register_craft({
	output = "paintings_lib:4x2_frost_punk",
	recipe = {
		{"paintings_lib:4x2_blank4x2", "dye:white"},
		{"", "dye:magenta"},
	}
})

minetest.register_craft({
	output = "paintings_lib:4x2_lily_pond",
	recipe = {
		{"paintings_lib:4x2_blank4x2", "dye:green"},
		{"", "dye:pink"},
	}
})

minetest.register_craft({
	output = "paintings_lib:4x2_lime_matrix",
	recipe = {
		{"paintings_lib:4x2_blank4x2", "dye:grey"},
		{"", "dye:green"},
	}
})

minetest.register_craft({
	output = "paintings_lib:4x2_purple_mountain",
	recipe = {
		{"paintings_lib:4x2_blank4x2", "dye:black"},
		{"", "dye:violet"},
	}
})

minetest.register_craft({
	output = "paintings_lib:4x2_rpg_cliff",
	recipe = {
		{"paintings_lib:4x2_blank4x2", "dye:brown"},
		{"", "dye:yellow"},
	}
})

-- 4x3

minetest.register_craft({
	output = "paintings_lib:4x3_bioluminescent_shore",
	recipe = {
		{"paintings_lib:4x3_blank4x3", "dye:cyan"},
		{"", "dye:violet"},
	}
})

minetest.register_craft({
	output = "paintings_lib:4x3_goodnight_greenery",
	recipe = {
		{"paintings_lib:4x3_blank4x3", "dye:dark_green"},
		{"", "dye:blue"},
	}
})

minetest.register_craft({
	output = "paintings_lib:4x3_landing_eagle",
	recipe = {
		{"paintings_lib:4x3_blank4x3", "dye:brown"},
		{"", "dye:white"},
	}
})

minetest.register_craft({
	output = "paintings_lib:4x3_princess_meeting",
	recipe = {
		{"paintings_lib:4x3_blank4x3", "dye:blue"},
		{"", "dye:dark_grey"},
	}
})

minetest.register_craft({
	output = "paintings_lib:4x3_water_background",
	recipe = {
		{"paintings_lib:4x3_blank4x3", "dye:cyan"},
		{"", "dye:white"},
	}
})

-- 4x4

minetest.register_craft({
	output = "paintings_lib:4x4_abyssal_relic",
	recipe = {
		{"paintings_lib:4x4_blank4x4", "dye:cyan"},
		{"", "dye:black"},
	}
})

minetest.register_craft({
	output = "paintings_lib:4x4_forest_music",
	recipe = {
		{"paintings_lib:4x4_blank4x4", "dye:green"},
		{"", "dye:brown"},
	}
})

minetest.register_craft({
	output = "paintings_lib:4x4_purple_maze",
	recipe = {
		{"paintings_lib:4x4_blank4x4", "dye:blue"},
		{"", "dye:violet"},
	}
})

minetest.register_craft({
	output = "paintings_lib:4x4_quantum_garden",
	recipe = {
		{"paintings_lib:4x4_blank4x4", "dye:dark_green"},
		{"", "dye:yellow"},
	}
})

minetest.register_craft({
	output = "paintings_lib:4x4_tress_gathered",
	recipe = {
		{"paintings_lib:4x4_blank4x4", "dye:green"},
		{"", "dye:dark_grey"},
	}
})

minetest.register_craft({
	output = "paintings_lib:4x4_trippy_shrooms",
	recipe = {
		{"paintings_lib:4x4_blank4x4", "dye:violet"},
		{"", "dye:orange"},
	}
})

		end
	end
end
