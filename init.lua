-- Paintings Gallery

paintings_gallery_ii = {}

local default_path = minetest.get_modpath("paintings_gallery_ii")

dofile(minetest.get_modpath("paintings_gallery_ii") .. "/paintings.lua")
dofile(minetest.get_modpath("paintings_gallery_ii") .. "/craft.lua")
